package com.training.utilities;

import com.training.model.Category;
import com.training.model.Pet;

public class PetMother {
    private static final String CATEGORY_NAME = "categoryName";
    private static final int CATEGORY_ID = 123;
    private static final Category CATEGORY = new Category(CATEGORY_NAME, CATEGORY_ID);
    private static final String STATUS = "available";
    public static final String PHOTO_URL = "www.awesome-photo.se";

    public static Pet withIdAndName(int id, String name){
        return new Pet.PetBuilder()
                .id(id)
                .name(name)
                .category(CATEGORY)
                .photoUrls(new String[]{PHOTO_URL})
                .status(STATUS)
                .tags(new Category[]{CATEGORY})
                .build();
    }
}