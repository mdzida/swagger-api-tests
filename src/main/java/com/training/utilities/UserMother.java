package com.training.utilities;

import com.training.model.User;

public class UserMother {
    private static final String DEFAULT_LASTNAME = "Bond";
    public static final String DEFAULT_EMAIL = "email@onet.pl";
    private static final String DEFAULT_PASSWORD = "password";
    private static final String DEFAULT_PHONE = "258745896";
    private static final long DEFAULT_USER_STATUS = 123;

    public static User withIdAndUsername(int id, String username) {
        return new User.Builder()
                .id(id)
                .username(username)
                .lastName(DEFAULT_LASTNAME)
                .email(DEFAULT_EMAIL)
                .password(DEFAULT_PASSWORD)
                .phone(DEFAULT_PHONE)
                .userStatus(DEFAULT_USER_STATUS)
                .build();
    }
}
