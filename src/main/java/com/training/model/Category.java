package com.training.model;

import lombok.Getter;
import lombok.Setter;

@Getter
public class Category {
    private long id;
    private String name;

    private Category() {
    }

    public Category(String name, long id) {
        this.name = name;
        this.id = id;
    }
}
