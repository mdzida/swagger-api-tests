package com.training.model;

import lombok.Getter;

@Getter
public class User {
    private int id;
    private String username;
    private String lastName;
    private String email;
    private String password;
    private String phone;
    private long userStatus;

    private User(Builder builder) {
        this.id = builder.id;
        this.username = builder.username;
        this.lastName = builder.lastName;
        this.password = builder.password;
        this.email = builder.email;
        this.phone = builder.phone;
        this.userStatus = builder.userStatus;
    }

    private User(){
    }

    public static class Builder {
        private int id;
        private String username;
        private String lastName;
        private String email;
        private String password;
        private String phone;
        private long userStatus;

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder username(String username) {
            this.username = username;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder userStatus(long userStatus) {
            this.userStatus = userStatus;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}