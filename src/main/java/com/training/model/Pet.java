package com.training.model;

import lombok.Getter;

@Getter
public class Pet {

    private long id;
    private Category category;
    private String name;
    private String[] photoUrls;
    private Category[] tags;
    private String status;

    Pet(long id, Category category, String name, String[] photoUrls, Category[] tags, String status) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.photoUrls = photoUrls;
        this.tags = tags;
        this.status = status;
    }

    private Pet(){

    }

    public static class PetBuilder {
        private long id;
        private Category category;
        private String name;
        private String[] photoUrls;
        private Category[] tags;
        private String status;

        public PetBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PetBuilder category(Category category) {
            this.category = category;
            return this;
        }

        public PetBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PetBuilder photoUrls(String[] photoUrls) {
            this.photoUrls = photoUrls;
            return this;
        }

        public PetBuilder tags(Category[] tags) {
            this.tags = tags;
            return this;
        }

        public PetBuilder status(String status) {
            this.status = status;
            return this;
        }

        public Pet build() {
            return new Pet(id, category, name, photoUrls, tags, status);
        }
    }
}
