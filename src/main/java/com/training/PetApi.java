package com.training;

import com.training.model.Pet;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class PetApi {

    private static final String BASE_URL = "http://petstore.swagger.io/v2/pet/";

    private RestTemplate restTemplate;

    public PetApi() {
        this.restTemplate = new RestTemplate();
    }

    public ResponseEntity<Pet> getPet(String petId) {
        String getPetUrl = BASE_URL + petId;

        return restTemplate.getForEntity(getPetUrl, Pet.class);
    }

    public ResponseEntity<Pet[]> getPetsByStatus(String status) {
        String findStatusSuffix = "findByStatus?status=";

        String getPetsByStatus = BASE_URL + findStatusSuffix + status;
        return restTemplate.getForEntity(getPetsByStatus, Pet[].class);
    }

    public ResponseEntity<Pet> addPetToStore(Pet petToBeAdded) {
        return restTemplate.postForEntity(BASE_URL, petToBeAdded, Pet.class);
    }

    public ResponseEntity<Pet> updatePet(Pet petToBeUpdated) {
        return restTemplate.postForEntity(BASE_URL, petToBeUpdated, Pet.class);
    }
}