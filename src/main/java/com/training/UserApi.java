package com.training;

import com.training.model.User;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class UserApi {

    private static final String BASE_URL = "http://petstore.swagger.io/v2/user/";
    private RestTemplate restTemplate;

    UserApi() {
        this.restTemplate = new RestTemplate();
    }

    public ResponseEntity<User> getUser(String username) {
        String getUserUrl = BASE_URL + username;

        return restTemplate.getForEntity(getUserUrl, User.class);
    }

    public ResponseEntity<Void> addUser(User userToAdd) {
        return restTemplate.postForEntity(BASE_URL, userToAdd, Void.class);
    }

    public ResponseEntity<Void> addUsersWithArray(User[] usersToAdd) {
        String createUsersWithArrayUrl = BASE_URL + "createWithArray";
        return restTemplate.postForEntity(createUsersWithArrayUrl, usersToAdd, Void.class);
    }

    public ResponseEntity<Void> updateUser(String username, User userToUpdate) {
        String updateUserUrl = BASE_URL + username;

        HttpEntity<User> requestEntity = new HttpEntity<>(userToUpdate);
        return restTemplate.exchange(updateUserUrl, HttpMethod.PUT, requestEntity, Void.class);
    }

    public ResponseEntity<Void> deleteUser(String username) {
        String deleteUserUrl = BASE_URL + username;

        HttpEntity<String> requestEntity = new HttpEntity<>(username);
        return restTemplate.exchange(deleteUserUrl, HttpMethod.DELETE, requestEntity, Void.class);
    }
}
