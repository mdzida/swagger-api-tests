package com.training;

import com.training.model.Category;
import com.training.model.Pet;
import com.training.utilities.PetMother;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

public class PetApiTest {

    private final PetApi petApi = new PetApi();

    @Test
    public void shouldGetPetByPetId() {
        int randomId = RandomUtils.nextInt(1, 1000);
        String randomName = RandomStringUtils.randomAlphabetic(10);

        Pet petToAdd = PetMother.withIdAndName(randomId, randomName);
        ResponseEntity<Pet> addPetResponse = petApi.addPetToStore(petToAdd);
        assertThat(addPetResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

        ResponseEntity<Pet> response = petApi.getPet(Integer.toString(randomId));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);

        Pet pet = response.getBody();
        assertThat(pet.getId()).isEqualTo(randomId);
        assertThat(pet.getName()).isEqualTo(randomName);
        assertThat(pet.getPhotoUrls()).contains(PetMother.PHOTO_URL);
    }

    @Test
    public void shouldGetPetByStatus() {
        String statusAv = "available";
        ResponseEntity<Pet[]> responseAv = petApi.getPetsByStatus(statusAv);

        assertThat(responseAv.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseAv.getStatusCodeValue()).isEqualTo(200);

        String statusPen = "pending";
        ResponseEntity<Pet[]> responsePen = petApi.getPetsByStatus(statusPen);
        assertThat(responsePen.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responsePen.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    public void shouldAddPetToStore() {
        int randomId = RandomUtils.nextInt(1, 1000);
        String randomName = RandomStringUtils.randomAlphabetic(10);

        Pet pet = PetMother.withIdAndName(randomId, randomName);
        ResponseEntity<Pet> response = petApi.addPetToStore(pet);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);

        ResponseEntity<Pet> getResponse = petApi.getPet(Integer.toString(randomId));
        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

        Pet addedPet = getResponse.getBody();
        assertThat(addedPet.getName()).isEqualTo(randomName);
        assertThat(addedPet.getId()).isEqualTo(randomId);
    }

    @Test
    public void shouldUpdatePet() {
        int randomId = RandomUtils.nextInt(1, 1000);
        String randomName = RandomStringUtils.randomAlphabetic(10);

        Pet petToUpdate = PetMother.withIdAndName(randomId, randomName);
        ResponseEntity<Pet> response = petApi.updatePet(petToUpdate);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        ResponseEntity<Pet> getResponse = petApi.getPet(Integer.toString(randomId));
        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

        Pet updatedPet = getResponse.getBody();
        assertThat(updatedPet.getName()).isEqualTo(randomName);
    }
}
