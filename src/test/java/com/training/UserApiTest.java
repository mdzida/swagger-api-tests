package com.training;

import com.training.model.User;
import com.training.utilities.UserMother;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.assertj.core.api.Java6Assertions.assertThat;

public class UserApiTest {

    private final UserApi userApi = new UserApi();

    @Test
    public void shouldAddUser() {
        int randomId = RandomUtils.nextInt(1, 100);
        String randomUsername = RandomStringUtils.randomAlphabetic(10);

        User userToAdd = UserMother.withIdAndUsername(randomId, randomUsername);

        ResponseEntity<Void> response = userApi.addUser(userToAdd);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);

        ResponseEntity<User> getResponse = userApi.getUser(randomUsername);
        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

        User addedUser = getResponse.getBody();
        assertThat(addedUser.getId()).isEqualTo(randomId);
        assertThat(addedUser.getEmail()).isEqualTo(UserMother.DEFAULT_EMAIL);
    }

    @Test
    public void shouldGetUserByUsername() {
        String username = "user1";
        int userId = 1;
        String userPhone = "123-456-7890";
        long userStatus = 1;

        ResponseEntity<User> response = userApi.getUser(username);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(200);

        User user = response.getBody();
        assertThat(user.getId()).isEqualTo(userId);
        assertThat(user.getPhone()).isEqualTo(userPhone);
        assertThat(user.getUserStatus()).isEqualTo(userStatus);
    }

    @Test
    public void shouldUpdateUser(){
        int previousId = 13;
        int updatedId = 24;
        String randomUsername = RandomStringUtils.randomAlphabetic(5);

        User userToBeUpdated = UserMother.withIdAndUsername(previousId, randomUsername);
        User updatedUser = UserMother.withIdAndUsername(updatedId, randomUsername);

        ResponseEntity<Void> addUserResponse = userApi.addUser(userToBeUpdated);
        assertThat(addUserResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

        ResponseEntity<Void> response = userApi.updateUser(randomUsername, updatedUser);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        ResponseEntity<User> getResponse = userApi.getUser(randomUsername);
        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

        User updatedUserFromDatabase = getResponse.getBody();
        assertThat(updatedUserFromDatabase.getUsername()).isEqualTo(randomUsername);
        assertThat(updatedUserFromDatabase.getId()).isEqualTo(updatedId);
    }

    @Test
    public void shouldDeleteUser(){
        final String randomName = RandomStringUtils.randomAlphabetic(10);
        int userId = 13;

        User userToDelete = UserMother.withIdAndUsername(userId, randomName);

        ResponseEntity<Void> response = userApi.addUser(userToDelete);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        ResponseEntity<Void> deleteResponse = userApi.deleteUser(randomName);
        assertThat(deleteResponse.getStatusCodeValue()).isEqualTo(200);

        assertThatThrownBy(() -> userApi.deleteUser(randomName)).hasMessageContaining("404 Not Found");
    }

    @Test
    public void shouldAddUsersWithArray(){
        final String firstRandomName = RandomStringUtils.randomAlphabetic(10);
        final String secondRandomName = RandomStringUtils.randomAlphabetic(10);
        int firstUserId = 54;
        int secondUserId = 40;

        User firstUser = UserMother.withIdAndUsername(firstUserId, firstRandomName);
        User secondUser = UserMother.withIdAndUsername(secondUserId, secondRandomName);

        User[] arrayOfUsers = {firstUser, secondUser};

        ResponseEntity<Void> response = userApi.addUsersWithArray(arrayOfUsers);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        ResponseEntity<User> getFirstResponse = userApi.getUser(firstRandomName);
        ResponseEntity<User> getSecondResponse = userApi.getUser(secondRandomName);

        assertThat(getFirstResponse.getBody().getUsername()).isEqualTo(firstRandomName);
        assertThat(getFirstResponse.getBody().getId()).isEqualTo(firstUserId);
        assertThat(getSecondResponse.getBody().getUsername()).isEqualTo(secondRandomName);
        assertThat(getSecondResponse.getBody().getId()).isEqualTo(secondUserId);
    }
}
